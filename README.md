# GreenLabNILM #



### What is GreenLabNILM? ###

**GreenLabNILM** (**N**on-**I**ntrusive **L**oad **M**onitoring) is an **IoT** solution for laboratory devices (microscopes, spectrometers, etc.) 
that automatically conserves energy consumption and taps into green renewable energy sources within an electric grid. 